package com.lai.mpadnroidchart;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.BarLineChartTouchListener;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    MyLineChart mLineChart;
    private ViewPortHandler mViewPortHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLineChart = findViewById(R.id.chart);

        //1.设置x轴和y轴的点
        List<Entry> entries = new ArrayList<>();
        entries.add(new Entry(0, 20));
        entries.add(new Entry(1, 70));
        entries.add(new Entry(2, 40));
        entries.add(new Entry(3, 50));
        entries.add(new Entry(4, 20));
        entries.add(new Entry(5, 90));
        entries.add(new Entry(6, 40));
        entries.add(new Entry(7, 60));
        entries.add(new Entry(8, 10));
        entries.add(new Entry(9, 80));
        entries.add(new Entry(10, 50));
        entries.add(new Entry(11, 70));

        //2.把数据赋值到你的线条
        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        dataSet.setDrawCircles(false);
        dataSet.setColor(Color.parseColor("#7d7d7d"));//线条颜色
        dataSet.setCircleColor(Color.parseColor("#7d7d7d"));//圆点颜色
        dataSet.setLineWidth(1f);//线条宽度
        dataSet.setHighlightEnabled(false);
        mLineChart.setScaleEnabled(false);

        //mLineChart.getLineData().getDataSets().get(0).setVisible(true);
        //设置样式
        YAxis rightAxis = mLineChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        //设置图表右边的y轴禁用
        rightAxis.setEnabled(true);
        YAxis leftAxis = mLineChart.getAxisLeft();
        //设置图表左边的y轴禁用
        leftAxis.setEnabled(true);
        leftAxis.setDrawGridLines(false);
        rightAxis.setAxisMaximum(dataSet.getYMax() * 2);
        leftAxis.setAxisMaximum(dataSet.getYMax() * 2);
        //设置x轴
        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setTextColor(Color.parseColor("#333333"));
        xAxis.setTextSize(11f);
        xAxis.setAxisMinimum(0f);
        xAxis.setDrawAxisLine(true);//是否绘制轴线
        xAxis.setDrawGridLines(false);//设置x轴上每个点对应的线
        xAxis.setDrawLabels(true);//绘制标签  指x轴上的对应数值
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//设置x轴的显示位置
        xAxis.setGranularity(1f);//禁止放大x轴标签重绘
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            list.add(String.valueOf(i + 1).concat("月"));
        }
        xAxis.setValueFormatter(new IndexAxisValueFormatter(list));
        xAxis.setLabelCount(12);

        //透明化图例
        Legend legend = mLineChart.getLegend();
        legend.setForm(Legend.LegendForm.NONE);
        legend.setTextColor(Color.WHITE);
        //legend.setYOffset(-2);
        //点击图表坐标监听


        //隐藏x轴描述
        Description description = new Description();
        description.setEnabled(true);
        description.setText("描述");
        mLineChart.setDescription(description);

        //3.chart设置数据
        LineData lineData = new LineData(dataSet);
        //是否绘制线条上的文字
        lineData.setDrawValues(false);
        mLineChart.setData(lineData);
        float scaleX = mLineChart.getScaleX();
        if (scaleX == 1)
            mLineChart.zoomToCenter(2f, 1f);
        else {
            BarLineChartTouchListener barLineChartTouchListener = (BarLineChartTouchListener) mLineChart.getOnTouchListener();
            barLineChartTouchListener.stopDeceleration();
            mLineChart.fitScreen();
        }
        mLineChart.highlightValue(new Highlight(mLineChart.getCenter().getX(), mLineChart.getY(), entries.size() / 2));
        mLineChart.invalidate(); // refresh
        mViewPortHandler = mLineChart.getViewPortHandler();
        viewToCenter();
        setListener();
    }

    private void setListener() {
        mLineChart.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                viewToCenter();
            }

            @Override
            public void onChartLongPressed(MotionEvent me) {

            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {

            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {
            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {
            }
        });

    }

    private void viewToCenter() {
        float centerVisibleXIndex = getCenterVisibleXIndex();
        float centerVisibleYValue = mLineChart.getData().getDataSets().get(0).getEntriesForXValue(centerVisibleXIndex).get(0).getY();
        mLineChart.centerViewToAnimated(centerVisibleXIndex, centerVisibleYValue, YAxis.AxisDependency.LEFT, 300);
    }

    public int getCenterVisibleXIndex() {
        float[] pts_left = new float[]{mViewPortHandler.contentLeft(),
                mViewPortHandler.contentBottom()};
        mLineChart.getTransformer(YAxis.AxisDependency.LEFT).pixelsToValue(pts_left);
        float[] pts_right = new float[]{mViewPortHandler.contentRight(),
                mViewPortHandler.contentBottom()};
        mLineChart.getTransformer(YAxis.AxisDependency.LEFT).pixelsToValue(pts_right);
        int pt_middle = Math.round(pts_left[0] + (pts_right[0] - pts_left[0]) / 2);
        if (pt_middle <= 0)
            pt_middle = 0;
        return pt_middle;
    }
}
