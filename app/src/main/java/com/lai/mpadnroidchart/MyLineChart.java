package com.lai.mpadnroidchart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.lai.mpadnroidchart.marker.DetailsMarkerView;
import com.lai.mpadnroidchart.marker.PositionMarker;
import com.lai.mpadnroidchart.marker.RoundMarker;

import java.lang.ref.WeakReference;

public class MyLineChart extends LineChart {

    public MyLineChart(Context context) {
        super(context);
    }

    public MyLineChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyLineChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void drawMarkers(Canvas canvas) {
        super.drawMarkers(canvas);
        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(1.5f);
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        canvas.drawLine(width / 2, 0, width / 2, height - 40, paint);
    }
}
